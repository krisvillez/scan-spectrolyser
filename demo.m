
path_in = '.\constat_uvvis\' ;
filename = '2006-01-01_00-02-00.fp' ;
timeformat = 'yyyy.mm.dd  HH:MM:SS';

addpath('tools')
[times_num,status,spectra] = getspectra_scan_constat(path_in,filename,timeformat);

figure, 
    plot(times_num,spectra(:,30),'.-')
    datetick('x','HH:MM:SS')