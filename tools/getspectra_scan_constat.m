function [times_num,status,spectra] = getspectra_scan_constat(path_in,filename,timeformat)

filepath_source = [path_in filename] ;

filepath_destin = [ filename(1:end-2) 'csv' ];
copyfile(filepath_source,filepath_destin)

tab=readtable(filepath_destin,'Delimiter','\t');

size(tab)

drawnow()
pause(.1)

delete(filepath_destin)

drawnow()
pause(.1)

if isempty(tab)
    times_num = [];
    status=[];;
    spectra=[]
else
    [nLine,nCol] = size(tab);
    
    times_num = nan(nLine,1);
    for iLine=1:nLine
        try
            times_str = table2array( tab(iLine,1) );
            times_num(iLine) = datenum(times_str,timeformat);
        catch
        end
    end
    include = find(~isnan(times_num));
    size(include)
    times_num = times_num(include);
    
%     figure(42),
%     stem(diff(times_num),'.-')
%     drawnow()
%     if any(diff(times_num)<=0)
%         pause
%     end
    
    tab = tab(include,:);
    [nLine,nCol] = size(tab);
    
    status = table2array( tab(:,2) );
    status = strcmp(status,'Ok') ;
    spectra = nan(nLine,nCol-2) ;
    for iCol=[3:nCol]
        y = table2array(tab(:,iCol));
        if isnumeric(y)
        else
            y = strrep(y,',','.');
            y = str2num(char(y));
        end
        spectra(:,iCol-2) =y ;
    end
    
end

